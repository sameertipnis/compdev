#include <iostream>
// This code is to understand rvalues, lvalues and teir references.
int GetRValue()
{
  // Function returning a rvalue
  return 10;
} 
int & GetLValueReference()
{
  // Function returning an lvalue reference
  static int i = 10;
  return i;
}
void setLValue( int i)
{
  // Function that takes lvalue as an argument - passing by copy
  i = i * 10;
}
void setLValueReference( int & i)
{
  // Function that takes lvalue reference as an regument - passing by reference
  i = i * 10;
} 
void setLValueConstReference ( const int & i )
{
  // Function that takes contant lvalue reference as an regument - passing by reference
  
} 

int main()
{
  int i = 10;  // i is a lvalue and 10 is a rvalue; lvalue is assigned rvalue of 10.
               // rvalue cannot be assigned a lvalue ; 10 = i is not a valid statement.
  
  int a = i ;  // a is an lvalue and i is also an lvalue - lvalue can be initialized/assigned an 
               // rvalue or another lvalues
  
  int c = GetRValue(); // c is an lvalue assigned an rvalue - rvalue does not have to be a literal it can be
                       // a value returned by a Function - as rvalue cannot be assigned value GetRValue() = 15 is invalid statement
  
  GetLValueReference() = 15;  // Function that returns a lvalue reference can be assigned a rvalue

  setLValue(c); setLValue(10);    // Set Value takes an lvalue which can be a lvalue or rvalue

  setLValueReference(c) ;   // lvalue reference can be taken from lvalue only
                            // lvalue reference cannot be taken from rvalues setLValueReference(10) is invalid
                            // int &i = 10; is also invalid
  setLValueConstReference (10); // It is valid that const lvalue reference can be taken from rvalue 

}
